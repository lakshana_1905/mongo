package net.codejava;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.opencsv.CSVWriter;
//import java.io.FileWriter;
import org.bson.Document;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class MongoDB {
	static public MongoDatabase db;
	static MongoClient mongoClient = null;
	public static void main(String[] args) throws IOException {
		String uri=System.getenv("key");
		
		
		String uri2;
		if(uri.equals(null)) {
			
			try {
				FileReader reader=new FileReader("src/main/resources/mongo.properties");  
			    Properties p=new Properties(); 
			    p.load(reader);  
			    uri2=p.getProperty("db");
			    mongoClient=MongoClients.create(uri2);
			    MyClass c=new MyClass();
			    c.FindAns();
			    
			}
			catch(Exception e) {
				System.out.println(e);
			}
			
		
		}
		else {
			mongoClient=MongoClients.create(uri);
			 MyClass c=new MyClass();
			c.FindAns();
		}
	}

}